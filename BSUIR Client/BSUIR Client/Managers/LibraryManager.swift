//
//  LibraryManager.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/3/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import UserNotifications

class LibraryManager {
    static var books = [String]()
    
    public static func getBooks() {
        RequestManager.getLibraryBooks(successBlock: { (books) in
            self.books = books
            self.createNotificationIfNeeded()
        }) { (error) in
            print(error)
        }
    }
    
    private static func createNotificationIfNeeded() {
        self.requestNotifications { (success) in
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["library_notification"])
            if books.count > 0 {
                let notification = UNMutableNotificationContent()
                notification.title = "Библиотека БГУИР"
                notification.subtitle = "Сдать книги до 15 июля"
                var notificationBody = "Список:\n"
                
                for book in 0..<books.count {
                    notificationBody += "- \(books[book])"
                    if book != books.count - 1 {
                        notificationBody += "\n"
                    }
                }
                notification.body = notificationBody
                let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)
                
                let notificationRequest = UNNotificationRequest(identifier: "library_notification", content: notification, trigger: notificationTrigger)
                
                // Add Request to User Notification Center
                UNUserNotificationCenter.current().add(notificationRequest) { (error) in
                    if let error = error {
                        print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
                    }
                }
            }
        }
    }
    
    private static func requestNotifications(completionHandler: @escaping (_ success: Bool) -> ()) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            
            completionHandler(success)
        }
    }
}
