//
//  RSSManager.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/6/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import FeedKit
import UIKit
import Kingfisher

class Event {
    
    static var events = [Event]()
    
    let title: String
    let date: String
    let url: String
    let dateNumber: String
    let type: String
    
    init(title: String, date: String, url: String, dateNumber: String, type: String) {
        self.title = title
        self.date = date
        self.url = url
        self.dateNumber = dateNumber
        self.type = type
    }
}

extension Event {
    static func generateEvents() {
        self.events = [Event(title: "Конкурс курсовых проектов \n'Course Battle 2018'", date: "Декабря", url: "http://fksis.bsuir.by/other-info/news/2018-11-22+news+course+battle+2018", dateNumber: "10", type: "Конкурс"),
                       Event(title: "Облачные сервисы Искусственного Интеллекта для разработчиков", date: "Декабря", url: "https://events.dev.by/oblachnye-servisy-iskusstvennogo-intellekta-dlya-razrabotchikov", dateNumber: "11", type: "Воркшоп"),
                       Event(title: "Xакатон Hackaton Light: IoT for Business", date: "Декабря", url: "https://docs.google.com/forms/d/e/1FAIpQLScU-KRPTFHfQ7gdsj_K551_Ng8b5xaUIh98f1D1jn49Tnl8cg/viewform", dateNumber: "14", type: "Хакатон"),
                       Event(title: "Бизнес-конференция «Как делать эффективные презентации?»", date: "Декабря", url: "https://events.dev.by/biznes-konferentsiya-kak-delat-effektivnye-prezentatsii", dateNumber: "15", type: "Конференция"),
                       Event(title: "Привлечение инвестиций: как не упустить возможность", date: "Декабря", url: "https://events.dev.by/privlechenie-investitsiy-kak-ne-upustit-vozmozhnost", dateNumber: "18", type: "Семинар"),
                       Event(title: "Конкурс видеороликов \nк 55-летию БГУИР ", date: "Января", url: "https://vk.com/bsuir_official?w=wall-143039548_4448", dateNumber: "25", type: "Конкурс")
        ]
    }
}
