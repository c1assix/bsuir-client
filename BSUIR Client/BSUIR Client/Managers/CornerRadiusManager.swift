//
//  CornerRadiusManager.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/5/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import UIKit

class CornerRadiusManager {
    static func corner(for corners: CACornerMask = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner], radius: CGFloat, view: UIView) {
        view.clipsToBounds = true
        view.layer.cornerRadius = radius
        view.layer.maskedCorners = corners
    }
    
}
