//
//  ParamsGenerator.swift
//  vpn-ios
//
//  Created by Лев Купчинов on 27.07.2018.
//  Copyright © 2018 Apps. All rights reserved.
//

import Foundation

class ParamsGenerator {

    static func generateLoginParamters(username: String, password: String) -> RequestManagerParams {
        let params = ["username": username, "password": password] as [String: Any]
        return params
    }
}
