//
//  DeviceRequestManager.swift
//  vpn-ios
//
//  Created by Лев Купчинов on 27.07.2018.
//  Copyright © 2018 Apps. All rights reserved.
//

import Alamofire
import SVProgressHUD

typealias RequestManagerParams = [String: Any]
typealias RequestManagerResponseBlock = ((Any) -> Void)
typealias RequestManagerErrorBlock = ((Error) -> Void)

extension RequestManager {
    static func login(_ params: RequestManagerParams, successBlock: RequestManagerResponseBlock?, errorBlock: RequestManagerErrorBlock?) {
        let urlString = "/auth/login"
        self.performRequestWithJsonResponse(urlString, method: .post, requestParams: params, successBlock: successBlock, errorBlock: errorBlock)
    }
    static func debs(_ params: RequestManagerParams, successBlock: RequestManagerResponseBlock?, errorBlock: RequestManagerErrorBlock?) {
        let urlString = "/portal/personalCV"
        self.performRequestWithJsonResponse(urlString, method: .get, requestParams: params, successBlock: successBlock, errorBlock: errorBlock)
    }
    
    static func getSchedule(_ params: RequestManagerParams? = nil, group: String,successBlock: RequestManagerResponseBlock?, errorBlock: RequestManagerErrorBlock?) {
        let urlString = "/studentGroup/schedule?studentGroup=\(group)"
        self.performRequestWithJsonResponse(urlString, method: .get, requestParams: params, successBlock: successBlock, errorBlock: errorBlock)
    }
    
    static func getMarkbook(_ params: RequestManagerParams? = nil, successBlock: RequestManagerResponseBlock?, errorBlock: RequestManagerErrorBlock?) {
        let urlString = "/portal/markbook"
        self.performRequestWithJsonResponse(urlString, method: .get, requestParams: params, successBlock: successBlock, errorBlock: errorBlock)
    }
    
    static func getAllGroups(_ params: RequestManagerParams? = nil, successBlock: RequestManagerResponseBlock?, errorBlock: RequestManagerErrorBlock?) {
        guard let baseURL = URL(string: APIConstants.baseUrl), let requestURL = URL(string: "\(baseURL)/groups") else { return }
        Alamofire.request(requestURL).responseJSON { (response) in
            if let error = response.result.error {
                errorBlock?(error)
            } else {
                let result = response.result.value as? [Any] ?? []
                successBlock?(result)
            }
        }
    }
    
    static func getLibraryBooks(successBlock: @escaping (([String]) -> Void), errorBlock: RequestManagerErrorBlock?) {
        guard let baseURL = URL(string: APIConstants.baseUrl), let requestURL = URL(string: "\(baseURL)/portal/library/debs") else { return }
        Alamofire.request(requestURL).responseJSON { (response) in
            if let error = response.result.error {
                errorBlock?(error)
            } else {
                let result = response.result.value as? [String] ?? []
                successBlock(result)
            }
        }
    }
    
    static func getCurrentWeek(successBlock: @escaping ((Int) -> Void), errorBlock: RequestManagerErrorBlock?) {
        guard let baseURL = URL(string: APIConstants.baseUrl), let requestURL = URL(string: "\(baseURL)/week") else { return }
        Alamofire.request(requestURL).responseJSON { (response) in
            if let error = response.result.error {
                errorBlock?(error)
            } else {
                let result = response.result.value as? Int ?? 1
                successBlock(result)
            }
        }
    }
}

class RequestManager {

    fileprivate static func performRequestWithJsonResponse(_ url: String,
                                                           method: HTTPMethod,
                                                           requestParams: Parameters?,
                                                           encoding: ParameterEncoding = URLEncoding.default,
                                                           headers: HTTPHeaders? = nil,
                                                           successBlock: RequestManagerResponseBlock?,
                                                           errorBlock: RequestManagerErrorBlock?,
                                                           authorizationToken: Bool = true) {

        guard let baseURL = URL(string: APIConstants.baseUrl), let requestURL = URL(string: "\(baseURL)\(url)") else { debugPrint("\(String(describing: self)): Incorrect URL: \(url)"); return }

        var requestHeaders: HTTPHeaders = ["Content-Type" : "application/json",
                                           "Accept" : "application/json, text/plain, */*",
                                           "Accept-Language": "ru"]

        if method == .get {
            Alamofire.request(requestURL, method: method, encoding: URLEncoding.default, headers: requestHeaders).responseJSON { (response) in
                debugPrint(response)
                self.handleResponse(response, successBlock, { (error) in
                    errorBlock?(error)
                })
                if response.error != nil {
                    debugPrint("ERROR")
                }
            }
        } else {

            let dataRequest = Alamofire.request(requestURL, method: method, parameters: requestParams, encoding: JSONEncoding.prettyPrinted, headers: requestHeaders)

            dataRequest.responseJSON { (response) in
                print(response)
                self.handleResponse(response, successBlock, { (error) in
                    errorBlock?(error)
                })
                if response.error != nil {
                    debugPrint("ERROR")
                }
            }
        }
    }

    private static func handleResponse(_ response: DataResponse<Any>, _ successBlock: RequestManagerResponseBlock?, _ errorBlock: RequestManagerErrorBlock?) {
        if let error = response.result.error {
            let nserror = NSError(domain: "", code: 404, userInfo: [NSLocalizedDescriptionKey: error.localizedDescription])
            errorBlock?(nserror)
        } else if let result = response.result.value {
            let json = result as! [String : Any]

            if let error_code = json["code"] as? Int,
                error_code != 0,
                let host = response.request?.url?.host,
                let message = json["message"] as? String {
                let error = NSError(domain: host, code: error_code, userInfo: [NSLocalizedDescriptionKey: message])
                if error_code != 801 && error_code != 802 {
                    SVProgressHUD.showError(withStatus: message)
                }
                errorBlock?(error)
            } else {
                successBlock?(result)
            }
        }
    }
}

