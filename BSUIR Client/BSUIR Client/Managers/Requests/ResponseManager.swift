//
//  ResponseManager.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/28/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import SwiftyJSON

class ResponseManager {
    static func responseStudent(from json: Any) -> Student? {
        let json = JSON(json).dictionaryObject
        let student: Student? = Student.object(withDictionary: json)
        return student
    }
    
    static func responseLogin(from json: Any, completion: @escaping (Bool, Error?) -> Void) {
        let json = JSON(json).dictionaryObject
        let loggedIn = json?["loggedIn"] as? Int ?? 0
        if loggedIn == 1 {
            completion(true, nil)
        } else {
            let error = NSError(domain: "https://journal.bsuir.by", code: 49, userInfo:[ NSLocalizedDescriptionKey: "Invalid Credentials"])
            completion(false, error)
        }
    }
    
    static func responseSchedule(from json: Any) -> [Schedule] {
        let json = JSON(json).dictionaryValue
        guard let schedule = json["schedules"]?.arrayValue else { return [] }
        let schedules: [Schedule] = schedule.compactMap({ return Schedule.object(withDictionary: $0.dictionaryObject) })
        return schedules
    }
    
    static func responseGroups(from json: Any, completion: @escaping ([Group]) -> Void){
        let json = JSON(json).arrayValue
        let groups: [Group] = json.compactMap({ return Group.object(withDictionary: $0.dictionaryObject) })
        completion(groups)
    }
    
    static func responseMarkbook(from json: Any, completion: @escaping (Markbook) -> Void) {
        let json = JSON(json).dictionaryValue
        let averageMarkString = json["averageMark"]?.stringValue ?? ""
        let quaterMarksDict = json["markPages"]?.dictionaryObject ?? [:]
        var quaterMarks: [QuaterMarks] = []
        for quater in 1...quaterMarksDict.count {
            let dict = quaterMarksDict["\(quater)"] as? [String: Any] ?? [:]
            if let quaterMarksLocal = QuaterMarks.object(withDictionary: dict, quater: quater) {
                if !quaterMarksLocal.marks.isEmpty {
                    quaterMarks.append(quaterMarksLocal)
                }
            }
        }
        let averageMark = Double(averageMarkString)
        let markbook = Markbook(averageMark: averageMark ?? 0.0, quaterMarks: quaterMarks)
        completion(markbook)
    }
}
