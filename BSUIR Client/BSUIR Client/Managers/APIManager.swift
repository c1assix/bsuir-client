//
//  APIManager.swift
//  vpn-ios
//
//  Created by Лев Купчинов on 27.07.2018.
//  Copyright © 2018 Apps. All rights reserved.
//

import Foundation

class APIManager {

    static private(set) var pushToken: String?
    static func pushToken(_ token: String) {
        self.pushToken = token
    }
}
