//
//  DataSource.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/28/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SelectedGroup {
    let groupNumber: String
    let subgroup: String
    
    init(groupNumber: String, subgroup: String) {
        self.groupNumber = groupNumber
        self.subgroup = subgroup
    }
}

class GroupsSource {
    static var allGroups = [Group]()
    
    static func removeGroupAt(_ index: Int) {
        let fetchRequest: NSFetchRequest<ScopedGroups> = ScopedGroups.fetchRequest()
        do {
            let groups = try PersistenceService.context.fetch(fetchRequest)
            PersistenceService.context.delete(groups[index])
            PersistenceService.saveContext()
        }
        catch {
            
        }

    }
    
    class var scopedGroups: [SelectedGroup] {
        get {
            let fetchRequest: NSFetchRequest<ScopedGroups> = ScopedGroups.fetchRequest()
            do {
                let groups = try PersistenceService.context.fetch(fetchRequest)
                var groupsArray = [SelectedGroup]()
                groups.forEach { (group) in
                    if let groupNumber = group.groupNumber, let subgroup = group.subGroupNumber {
                        let selectedGroup = SelectedGroup(groupNumber: groupNumber, subgroup: subgroup)
                        groupsArray.append(selectedGroup)
                    }
                }
                return groupsArray
            }
            catch {
                return []
            }
        }
        set {
            if let scopedGroup = newValue.last {
                let coreDataGroup = ScopedGroups(context: PersistenceService.context)
                coreDataGroup.groupNumber = scopedGroup.groupNumber
                coreDataGroup.subGroupNumber = scopedGroup.subgroup
                PersistenceService.saveContext()
            }
        }
    }
    
    class var selectedGroup: SelectedGroup? {
        get {
            guard let selectedGroupDict = UserDefaults.standard.dictionary(forKey: "selectedGroup") else { return nil }
            let groupNumber = selectedGroupDict["groupNumber"] as? String ?? ""
            let subgroup = selectedGroupDict["subgroupNumber"] as? String ?? ""
            return SelectedGroup(groupNumber: groupNumber, subgroup: subgroup)
        }
        set {
            if let value = newValue {
                let selectedGroupDict = ["groupNumber": value.groupNumber, "subgroupNumber": value.subgroup] as [String: Any]
                UserDefaults.standard.set(selectedGroupDict, forKey: "selectedGroup")
            } else {
                UserDefaults.standard.removeObject(forKey: "selectedGroup")
            }
        }
    }
}

class DataSource {
    static var student: Student?
    static var currentWeek: Int = 1
    static var studentMarkbook: Markbook?
    
    static var schedules = [Schedule]()
    
    static var currentSchedule = [Schedule]()
    
    static func addWeek(subgroup: Int) {
        for weekNumber in 1...4 {
            var week = [Schedule]()
            for day in 0..<schedules.count {
                var subjects = schedules[day].subjects
                subjects = subjects.filter({ (subgroup == $0.subGroupNumber || $0.subGroupNumber == 0 ) && $0.weekNumber.contains(weekNumber)})
                let day = Schedule(weekDay: schedules[day].weekDay, subjects: subjects)
                day.weekNumber = weekNumber
                if !day.subjects.isEmpty {
                    week.append(day)
                }
            }
            currentSchedule.append(contentsOf: week)
        }
    }
    
    static func deleteAuthCredits() {
        UserDefaults.standard.removeObject(forKey: "student.username")
        UserDefaults.standard.removeObject(forKey: "student.password")
    }
    
    static func saveAuthCredits(username: String, password: String) {
        UserDefaults.standard.set(username, forKey: "student.username")
        UserDefaults.standard.set(password, forKey: "student.password")

    }
    
    class var username: String? {
        get {
            return UserDefaults.standard.string(forKey: "student.username")
        }
        set {
        }
    }
    
    class var password: String? {
        get {
            return UserDefaults.standard.string(forKey: "student.password")
        }
        set {
        }
    }
}

class ProfileSource {
    static var items: [[ProfileItem]] = [[ProfileItem(title: "Зачетка", index: .markbook, image: UIImage(named: "markbook")!),
                                       ProfileItem(title: "Библиотека", index: .library, image: UIImage(named: "books")!)],
                                         [ProfileItem(title: "Заказать справку", index: .spravka, image: UIImage(named: "spravka"))], [ProfileItem(title: "Карта", index: .map, image: UIImage(named: "location"))],
                                         [ProfileItem(title: "Выйти", index: .logout)]]
}
