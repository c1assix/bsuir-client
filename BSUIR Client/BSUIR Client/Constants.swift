//
//  Constants.swift
//  vpn-ios
//
//  Created by Лев Купчинов on 27.07.2018.
//  Copyright © 2018 Apps. All rights reserved.
//

import Foundation
import UIKit

enum APIConstants {
    // MARK: Development configuration
    static let baseUrl = "https://journal.bsuir.by/api/v1"
    
}
