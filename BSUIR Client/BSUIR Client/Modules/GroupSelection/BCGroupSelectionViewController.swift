//
//  BCGroupSelectionViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/4/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import UIKit

class BCGroupSelectionViewController: UIViewController {
    
    @IBOutlet private weak var headerViewCard: UIView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var footerViewCard: UIView!
    @IBOutlet private weak var addGroupButton: LoadingButton!
    
    
    override func viewDidLoad() {
        CornerRadiusManager.corner(for: [.layerMaxXMinYCorner, .layerMinXMinYCorner], radius: 10, view: self.headerViewCard)
        CornerRadiusManager.corner(for: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 10, view: self.footerViewCard)
        self.loadGroupsIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        }
    }
    
    @IBAction func dismissClearButtonAction(_ sender: UIButton) {
        self.dismissAction()
    }
    
    @IBAction func addGroupAction(_ sender: Any) {
        self.showAlertWithGroups(with: GroupsSource.allGroups)
    }
    
    private func dismissAction(needNotification: Bool = false) {
        UIView.animate(withDuration: 0.2, animations: {
            self.view.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        }) { (isEnd) in
            self.dismiss(animated: true, completion: {
                if needNotification {
                    NotificationCenter.default.post(name: .updateSchedule, object: nil)
                }
            })
        }
    }

    private func loadGroupsIfNeeded() {
        if GroupsSource.allGroups.isEmpty {
            self.addGroupButton.showLoading()
            RequestManager.getAllGroups(successBlock: { (result) in
                ResponseManager.responseGroups(from: result, completion: { (groups) in
                    GroupsSource.allGroups = groups
                    GroupsSource.allGroups.sorted(by: { $0.name < $1.name })
                    self.addGroupButton.hideLoading()
                })
            }) { (error) in
                
            }
        }
    }
    
    private func showAlertWithGroups(with groups: [Group]) {
        let alert = UIAlertController(style: .alert, title: "Группы", message: "Выберете группу и подгруппу")
        var groupsString = [String]()
        let sortedGroups = groups.sorted(by: { $0.name < $1.name })
        let subgroups = ["1", "2"]
        sortedGroups.forEach { (group) in
            groupsString.append(group.name)
        }
        var selectedGroup: SelectedGroup?
        var groupNumber: String?
        var subgroupNumber: String = "1"
        let frameSizes: [CGFloat] = (150...600).map { CGFloat($0) }
        let pickerViewValues: [[String]] = [groupsString, subgroups]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 0)
        
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            if index.column == 0 {
                groupNumber = pickerViewValues[0][index.row]
            } else if index.column == 1 {
                subgroupNumber = pickerViewValues[1][index.row]
            }
            if let groupNumber = groupNumber {
                selectedGroup = SelectedGroup(groupNumber: groupNumber, subgroup: subgroupNumber)
            }
        }
        alert.addAction(title: "Добавить",style: .cancel) { (action) in
            if action.title == "Добавить" {
                if let group = selectedGroup {
                    GroupsSource.scopedGroups.append(group)
                    self.tableView.insertRows(at: [IndexPath(row: GroupsSource.scopedGroups.count - 1, section: 0)], with: .top)
                }
            }
        }
        alert.addAction(title: "Отмена", style: .destructive) { (action) in
            
        }
        print(alert.actions.count)
        alert.show(viewController: self)
    }
    
}

extension BCGroupSelectionViewController: UITableViewDelegate, UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = GroupsSource.scopedGroups[indexPath.row].groupNumber + "/" + String(GroupsSource.scopedGroups[indexPath.row].subgroup)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        GroupsSource.selectedGroup = GroupsSource.scopedGroups[indexPath.row]
        self.dismissAction(needNotification: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GroupsSource.scopedGroups.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            GroupsSource.removeGroupAt(indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .top)
        }
    }
}

extension BCGroupSelectionViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print(scrollView.contentOffset.y)
        if scrollView.contentOffset.y < -100 {
            self.dismissAction()
        }
    }
}

class LoadingButton: UIButton {
    var activityIndicator: UIActivityIndicatorView!
    
    func showLoading() {
        self.setImage(nil, for: .normal)
        self.setTitle("", for: .normal)
        
        if (activityIndicator == nil) {
            activityIndicator = createActivityIndicator()
        }
        self.isUserInteractionEnabled = false
        
        showSpinning()
    }
    
    func hideLoading() {
        self.setImage(UIImage(named: "Add"), for: .normal)
        self.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
    }
    
    private func createActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .lightGray
        return activityIndicator
    }
    
    private func showSpinning() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    private func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }
}
