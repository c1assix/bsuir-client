//
//  ViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/21/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import RevealingSplashView
import UserNotifications

class BCNewsViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "bsuir_logo_white")!,iconInitialSize: CGSize(width: 140, height: 77), backgroundColor: UIColor(red: 31.0/255.0, green: 57.0/255.0, blue: 143.0/255.0, alpha: 1.0))

        self.tabBarController?.view.addSubview(revealingSplashView)
        
        revealingSplashView.startAnimation()
        tableView.register(UINib(nibName: "CBNewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
        self.tableView.tableFooterView = UIView()
    }
    
    
}

extension BCNewsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "newsCell") as! CBNewsTableViewCell
        cell.setupCell(with: News.news[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushEventViewController(with: News.news[indexPath.row].url)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return News.news.count
    }
    
}

extension BCNewsViewController {
    private func pushEventViewController(with url: String) {
        let eventStoryboard = UIStoryboard(name: "BCEventViewController", bundle: nil)
        let eventViewController = eventStoryboard.instantiateInitialViewController() as! BCEventViewController
        eventViewController.requestUrlString = url
        self.navigationController?.pushViewController(eventViewController, animated: true)
    }
}
