//
//  BCLoginViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/28/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import SVProgressHUD

class BCLoginViewController: UIViewController {
    
    @IBOutlet private weak var stackTextField: UIStackView!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.usernameTextField.placeholder(text: "Номер зачетки", color: .blightGrey)
        self.passwordTextField.placeholder(text: "Пароль", color: .blightGrey)
    }
    
}

extension BCLoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.loginAction()
        self.passwordTextField.becomeFirstResponder()
        self.passwordTextField.endEditing(true)
        return true
    }
}

extension BCLoginViewController {
    private func loginAction() {
        SVProgressHUD.show()
        if let username = usernameTextField.text, let password = passwordTextField.text {
            RequestManager.login(ParamsGenerator.generateLoginParamters(username: username, password: password), successBlock: { (result) in
                ResponseManager.responseLogin(from: result, completion: { (loggedIn, error) in
                    if loggedIn {
                        RequestManager.debs(["":""], successBlock: { (result) in
                            DataSource.student = ResponseManager.responseStudent(from: result)
                            DataSource.saveAuthCredits(username: username, password: password)
                            self.presentMainStoryboard()
                            SVProgressHUD.dismiss()
                            self.usernameTextField.text = ""
                            self.passwordTextField.text = ""
                        }, errorBlock: { (error) in
                        })
                    }
                    if let error = error {
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                    }
                })
            }) { (error) in
                
            }
        } else {
            SVProgressHUD.showError(withStatus: "Ошибка")
        }
    }
    
    private func presentMainStoryboard() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let mainViewController = mainStoryboard.instantiateInitialViewController() else { return }
        mainViewController.modalTransitionStyle = .crossDissolve
        self.present(mainViewController, animated: true) {}
    }
}

extension BCLoginViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BCLoginViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
