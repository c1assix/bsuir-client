//
//  BCRootViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/28/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit

class BCRootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // self.presentMainStoryboard()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.authUser()
        RequestManager.getCurrentWeek(successBlock: { (currentWeek) in
            DataSource.currentWeek = currentWeek
        }) { (error) in
            print(error)
        }
        News.generateNews()
        Event.generateEvents()
    }
    
}

extension BCRootViewController {
    
    private func authUser() {
        if let username = DataSource.username, let password = DataSource.password {
            RequestManager.login(ParamsGenerator.generateLoginParamters(username: username, password: password), successBlock: { (result) in
                ResponseManager.responseLogin(from: result, completion: { (loggedIn, error) in
                    if loggedIn {
                        RequestManager.debs(["":""], successBlock: { (result) in
                            if let student = ResponseManager.responseStudent(from: result) {
                                DataSource.student = student
                                self.getSchedule(student: student, completion: {
                                    self.presentMainStoryboard()
                                })
                            }
                        }, errorBlock: { (error) in
                            print(error.localizedDescription)
                        })
                    } else {
                        print(error?.localizedDescription)
                    }
                })
            }) { (error) in
                print("error")
            }
        } else {
            self.presentLoginViewController()
        }
    }
    
    private func getSchedule(student: Student, completion: @escaping () -> Void) {
        var groupNumber: String = String(student.studentGroup)
        var subgroupNumber: Int = 1
        if let group = GroupsSource.selectedGroup {
            groupNumber = group.groupNumber
            subgroupNumber = Int(group.subgroup) ?? 1
        }
        RequestManager.getSchedule(group: groupNumber, successBlock: { (result) in
            DataSource.schedules = ResponseManager.responseSchedule(from: result)
            DataSource.addWeek(subgroup: subgroupNumber)
            completion()
        }) { (error) in
        }
    }
    
    private func presentLoginViewController() {
        let loginStoryboard = UIStoryboard(name: "BCLoginViewController", bundle: nil)
        guard let loginViewController = loginStoryboard.instantiateInitialViewController() else { return }
        loginViewController.modalTransitionStyle = .crossDissolve
        self.present(loginViewController, animated: true)
    }
    
    private func presentMainStoryboard() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let mainViewController = mainStoryboard.instantiateInitialViewController() else { return }
        mainViewController.modalTransitionStyle = .crossDissolve
        self.present(mainViewController, animated: false) {}
    }
}
