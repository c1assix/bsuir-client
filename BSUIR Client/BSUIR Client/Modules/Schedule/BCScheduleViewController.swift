//
//  BCScheduleViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/30/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import SVProgressHUD

class BCScheduleViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var groupNumberBarButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CBScheduleTableViewCell", bundle: nil), forCellReuseIdentifier: "scheduleCell")
        self.setBarButton()
        self.updateBarButtonTitle()
        NotificationCenter.default.addObserver(self, selector: #selector(updateScheduleIfNeeded), name: .updateSchedule, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    private func setBarButton() {
        self.groupNumberBarButton.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .semibold)], for: .normal)
        self.groupNumberBarButton.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .semibold)], for: .highlighted)
    }
    
    private func updateBarButtonTitle() {
        if let selectedGroup = GroupsSource.selectedGroup {
            self.groupNumberBarButton.title = "\(selectedGroup.groupNumber)/\(selectedGroup.subgroup)"
        } else if let student = DataSource.student{
            GroupsSource.selectedGroup = SelectedGroup(groupNumber: String(student.studentGroup), subgroup: "1")
            self.groupNumberBarButton.title = "\(student.studentGroup)/1"
        }
    }
    
    private func updateSchedule(with group: SelectedGroup) {
        SVProgressHUD.show()
        RequestManager.getSchedule(group: group.groupNumber, successBlock: { (result) in
            DataSource.schedules = ResponseManager.responseSchedule(from: result)
            DataSource.currentSchedule = []
            DataSource.addWeek(subgroup: Int(group.subgroup) ?? 1)
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            SVProgressHUD.dismiss()
        }) { (error) in
            
        }
    }
    
    @objc private func updateScheduleIfNeeded() {
        if let selectedGroup = GroupsSource.selectedGroup, selectedGroup.groupNumber + "/" + selectedGroup.subgroup != self.groupNumberBarButton.title {
            self.updateBarButtonTitle()
            self.updateSchedule(with: selectedGroup)
        }
    }
    
    
    @IBAction func selectGroupNumberAction(_ sender: Any) {
        self.presentGroupSelectionViewController()
    }
    
}

extension BCScheduleViewController: UITableViewDataSource, UITableViewDelegate {

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleCell") as! CBScheduleTableViewCell
        cell.styleCell(subject: DataSource.currentSchedule[indexPath.section].subjects[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return DataSource.currentSchedule[section].weekDay.rawValue + " Неделя " + String(DataSource.currentSchedule[section].weekNumber)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView {
            header.textLabel!.font = UIFont.systemFont(ofSize: 15.0, weight: .regular)
            header.textLabel!.textColor = UIColor.blightGrey
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DataSource.currentSchedule.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataSource.currentSchedule[section].subjects.count
    }
    
}

extension BCScheduleViewController {
    private func presentGroupSelectionViewController() {
        let groupSelectionStoryboard = UIStoryboard(name: "BCGroupSelectionViewController", bundle: nil)
        let groupSelectionViewController = groupSelectionStoryboard.instantiateInitialViewController()!
        groupSelectionViewController.modalPresentationStyle = .overCurrentContext
        self.tabBarController?.present(groupSelectionViewController, animated: true)
    }
}

