//
//  BCTabBarViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/3/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import SVProgressHUD

class BCTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        LibraryManager.getBooks()
        
        RequestManager.getMarkbook(successBlock: { (result) in
            ResponseManager.responseMarkbook(from: result, completion: { (markbook) in
                DataSource.studentMarkbook = markbook
            })
        }) { (error) in
            error
        }
        self.tabBar.items?[2].image = UIImage(named: "tabbar-schedule")
        self.tabBar.items?[2].selectedImage = UIImage(named: "tabbar-schedule-selected")
        self.tabBar.items?[3].image = UIImage(named: "tabbar-profile")
        self.tabBar.items?[3].selectedImage = UIImage(named: "tabbar-profile-selected")
        self.tabBar.items?[0].image = UIImage(named: "tabbar-news")
        self.tabBar.items?[0].selectedImage = UIImage(named: "tabbar-news-selected")
        self.tabBar.items?[1].image = UIImage(named: "tabbar-events")
        self.tabBar.items?[1].selectedImage = UIImage(named: "tabbar-events-selected")
        self.setButtonStates(itemTag: 1)
    }
    
    //choose normal and selected fonts here
    let normalTitleFont = UIFont.systemFont(ofSize: 11, weight: .regular)
    let selectedTitleFont = UIFont.systemFont(ofSize: 11, weight: .bold)
    
    //choose normal and selected colors here
    let normalTitleColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
    let selectedTitleColor = UIColor(red: 0.01, green: 0.17, blue: 0.71, alpha: 1.0)
    
    //the following is a delegate method from the UITabBar protocol that's available
    //to UITabBarController automatically. It sends us information every
    //time a tab is pressed. Since we Tagged our tabs earlier, we'll know which one was pressed,
    //and pass that identifier into a function to set our button states for us
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        setButtonStates(itemTag: item.tag)
    }
    
    
    //the function takes the tabBar.tag as an Int
    func setButtonStates (itemTag: Int) {
        
        //making an array of all the tabs
        let tabs = self.tabBar.items
        
        //looping through and setting the states
        var x = 0
        while x < (tabs?.count)! {
            
            if tabs?[x].tag == itemTag {
                tabs?[x].setTitleTextAttributes([NSAttributedStringKey.font: selectedTitleFont, NSAttributedStringKey.foregroundColor: selectedTitleColor], for: UIControlState.normal)
            } else {
                tabs?[x].setTitleTextAttributes([NSAttributedStringKey.font: normalTitleFont, NSAttributedStringKey.foregroundColor: normalTitleColor], for: UIControlState.normal)
            }
            
            x += 1
            
        }
        
    }
    
}

class BCUITabBarItemSubclass: UITabBarItem {
    
    //choose initial state fonts and weights here
    let normalTitleFont = UIFont.systemFont(ofSize: 11, weight: .regular)
    let selectedTitleFont = UIFont.systemFont(ofSize: 12, weight: .bold)
    
    //choose initial state colors here
    let normalTitleColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
    let selectedTitleColor = UIColor(red: 0.01, green: 0.17, blue: 0.71, alpha: 1.0)
    
    //assigns the proper initial state logic when each tab instantiates
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //this tag # should be your primary tab's Tag*
        if self.tag == 1 {
            self.setTitleTextAttributes([NSAttributedStringKey.font: selectedTitleFont, NSAttributedStringKey.foregroundColor: selectedTitleColor], for: UIControlState.normal)
        } else {
            self.setTitleTextAttributes([NSAttributedStringKey.font: normalTitleFont, NSAttributedStringKey.foregroundColor: normalTitleColor], for: UIControlState.normal)
        }
        
    }
    
}
