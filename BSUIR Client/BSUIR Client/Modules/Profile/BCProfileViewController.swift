//
//  BCProfileViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/28/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import Kingfisher

class BCProfileViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var userImage: UIImageView!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var specialityLabel: UILabel!
    @IBOutlet private weak var courseLabel: UILabel!
    @IBOutlet private weak var averageMarkLable: UILabel!
    @IBOutlet var stars: [UIImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.downloadAndSetupImage()
        self.styleView()
        self.setupStars()
        tableView.register(UINib(nibName: "CBProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "CBLogoutTableViewCell", bundle: nil), forCellReuseIdentifier: "logout")
        self.tableView.tableFooterView = UIView()
    }
    
    private func setupStars() {
        self.stars.forEach { (star) in
            star.tintColor = .blightGrey
        }
        if let student = DataSource.student {
            for index in 0..<student.rating {
                self.stars[index].tintColor = UIColor(red: 0.0/255.0, green: 142.0/255.0, blue: 252.0/255.0, alpha: 1.0)
            }
        }
    }
    
    private func downloadAndSetupImage() {

        if let student = DataSource.student {
            self.usernameLabel.text = student.lastName + " " + student.firstName
            self.courseLabel.text = "\(student.faculty) • \(student.course) курс"
            if student.photoUrl == nil {
                self.userImage.image = UIImage(named: "profile-noimage")
            } else {
                self.userImage.kf.setImage(with: student.photoUrl)
            }
        }
        
        if let markbook = DataSource.studentMarkbook {
            self.averageMarkLable.text = String(format: "%0.1f", markbook.averageMark)
        }
    }
    
    private func styleView() {
        self.userImage.layer.cornerRadius = self.userImage.bounds.height / 2
        self.userImage.clipsToBounds = true
    }
}

extension BCProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ProfileSource.items[indexPath.section][indexPath.row].index == .logout {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "logout") as! CBLogoutTableViewCell
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! CBProfileTableViewCell
            cell.styleCell(item: ProfileSource.items[indexPath.section][indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch ProfileSource.items[indexPath.section][indexPath.row].index {
        case .library:
            self.pushLibrary()
        case .markbook:
            self.pushMarkBook()
        case .logout:
            DataSource.deleteAuthCredits()
            self.tabBarController?.dismiss(animated: true)
        case .spravka:
            self.pushSpravka()
        case .map:
            self.pushMap()
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ProfileSource.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProfileSource.items[section].count
    }
    
}

extension BCProfileViewController {
    private func pushMarkBook() {
        let markbookStoryboard = UIStoryboard(name: "BCMarkbookViewController", bundle: nil)
        let markbookViewController = markbookStoryboard.instantiateInitialViewController()!
        self.navigationController?.pushViewController(markbookViewController, animated: true)
    }
    
    private func pushMap() {
        let markbookStoryboard = UIStoryboard(name: "MapViewController", bundle: nil)
        let markbookViewController = markbookStoryboard.instantiateInitialViewController()!
        self.navigationController?.pushViewController(markbookViewController, animated: true)
    }
    
    
    private func pushLibrary() {
        let libraryStoryboard = UIStoryboard(name: "BCLibraryViewController", bundle: nil)
        let libraryViewController = libraryStoryboard.instantiateInitialViewController()!
        self.navigationController?.pushViewController(libraryViewController, animated: true)
    }
    
    private func pushSpravka() {
        let libraryStoryboard = UIStoryboard(name: "BCCertificateViewController", bundle: nil)
        let libraryViewController = libraryStoryboard.instantiateInitialViewController()!
        self.navigationController?.pushViewController(libraryViewController, animated: true)
    }
}
