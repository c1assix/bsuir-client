//
//  BCEventViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/6/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit

class BCEventsViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var eventTypeButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "CBEventTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.tableView.tableFooterView = UIView()
    }
    
    @IBAction func selectEventTypeAction(_ sender: Any) {
        let alert = UIAlertController(style: .alert, title: "Title", message: "Message")
        alert.addAction(title: "Семинары", color: .black, style: .default) { action in
        }
        alert.addAction(title: "Хакатоны", color: .black, style: .default) { action in
        }
        alert.addAction(title: "Митапы", color: .black, style: .default) { action in
        }
        alert.show(viewController: self, animated: true)
    }
    
}

extension BCEventsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! CBEventTableViewCell
        cell.styleCell(with: Event.events[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.pushEventViewController(with: Event.events[indexPath.row].url)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Event.events.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension BCEventsViewController {
    private func pushEventViewController(with url: String) {
        let eventStoryboard = UIStoryboard(name: "BCEventViewController", bundle: nil)
        let eventViewController = eventStoryboard.instantiateInitialViewController() as! BCEventViewController
        eventViewController.requestUrlString = url
        self.navigationController?.pushViewController(eventViewController, animated: true)
    }
}
