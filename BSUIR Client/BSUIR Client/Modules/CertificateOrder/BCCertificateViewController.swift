//
//  BCCertificateViewController.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/7/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import SVProgressHUD

class BCCertificateViewController: UIViewController {

    @IBOutlet private weak var certificateButton: UIButton!
    @IBOutlet private weak var placeButton: UIButton!
    @IBOutlet private weak var orderButton: UIButton!
    
    private var components = [String]()
    private var type = ["Обычная", "С гербовой печатью"]
    private var reason = ["в банк", "в пункт приема донорской крови", "в ЖКХ", "в налоговую инспекцию", "в ОГиМ", "в посольство", "в ФСЗН", "в школу", "по месту работы", "по месту работы родителей", "по месту учебы сестры", "по месту учебы брата", "в паспортный стол",]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.styleDefaultsButtons()
        self.orderButton.alpha = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func styleDefaultsButtons() {
        self.certificateButton.layer.borderWidth = 4
        self.certificateButton.layer.borderColor = UIColor.darkBlue.cgColor
        self.certificateButton.setTitleColor(.darkBlue, for: .normal)
        self.placeButton.layer.borderWidth = 4
        self.placeButton.layer.borderColor = UIColor.darkBlue.cgColor
        self.placeButton.setTitleColor(.darkBlue, for: .normal)
    }
    
    private func createAndShowTypeAlert() {
        let alert = UIAlertController(style: .alert, title: "Вид справки", message: "")
        var selectedType: String = "Обычная"
        let pickerViewValues: [[String]] = [type]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 0)
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue, height: 100) { vc, picker, index, values in
            print(vc.preferredContentSize.height)
            selectedType = pickerViewValues[0][index.row]
        }
        alert.addAction(title: "Ок", style: .cancel) { (alertAction) in
                    self.certificateButton.setTitle(selectedType, for: .normal)
                    self.certificateButton.backgroundColor = .darkBlue
                    self.certificateButton.setTitleColor(.white, for: .normal)
                    self.showOrderButtonIfNeeded()
        }
        alert.show(viewController: self)
    }
    
    private func createAndShowPlaceAlert() {
        let alert = UIAlertController(style: .alert, title: "Место предостваления", message: "")
        var selectedType: String = "в школу"
        let pickerViewValues: [[String]] = [reason]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 7)
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue, height: 200) { vc, picker, index, values in
            print(vc.preferredContentSize.height)
            selectedType = pickerViewValues[0][index.row]
        }
        alert.addAction(title: "Ок", style: .cancel) { (alertAction) in
            self.placeButton.setTitle(selectedType, for: .normal)
            self.placeButton.backgroundColor = .darkBlue
            self.placeButton.setTitleColor(.white, for: .normal)
            self.showOrderButtonIfNeeded()
        }
        alert.show(viewController: self)
    }
    
    private func showOrderButtonIfNeeded() {
        if self.placeButton.backgroundColor == .darkBlue && self.certificateButton.backgroundColor == .darkBlue {
            UIView.animate(withDuration: 0.5) {
                self.orderButton.alpha = 1.0
            }
        }
    }
    
    @IBAction func orderAction(_ sender: Any) {
        SVProgressHUD.show()
        let darkView = UIView()
        darkView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        darkView.frame = self.tabBarController?.view.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)
        self.tabBarController?.view.addSubview(darkView)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            SVProgressHUD.showSuccess(withStatus: "Ваша справка успешно заказана")
            darkView.removeFromSuperview()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func setCertificateAction(_ sender: Any) {
        self.createAndShowTypeAlert()
    }
    
    @IBAction func setPlaceAction(_ sender: Any) {
        self.createAndShowPlaceAlert()
    }
}
