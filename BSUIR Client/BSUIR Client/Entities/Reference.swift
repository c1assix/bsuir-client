//
//  Reference.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/30/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

class Reference {
    let id: Int
    let name: String
    let reference: URL?
    
    init(id: Int, name: String, reference: URL?) {
        self.id = id
        self.name = name
        self.reference  = reference
    }
}
extension Reference {
    static func object(withDictionary dictionary: [String: Any]?) -> Reference? {
        guard let dictionary = dictionary else { return nil }
        var reference: URL?
        let referenceString = dictionary["reference"] as? String
        if let referenceString = referenceString {
            reference = URL(string: referenceString)
        }
        return Reference(id: dictionary["id"] as? Int ?? 0,
                         name: dictionary["name"] as? String ?? "",
                         reference: reference)
    }
}
