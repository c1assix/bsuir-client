//
//  Mark.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/6/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

enum FormOfControl : String{
    case exam = "Экзамен"
    case offset = "Зачет"
    case courseWork = "Курсовая работа"
    case courseProject = "Курсовой проект"
    case unknown = ""
}

class Mark {
    let subject: String
    let formOfControl: FormOfControl
    let mark: String
    let date: String
    let teacher: String
    
    init(subject: String, formOfControl: FormOfControl, mark: String, date: String, teacher: String) {
        self.subject = subject
        self.formOfControl = formOfControl
        self.mark = mark
        self.date = date
        self.teacher = teacher
    }
}

extension Mark {
    static func object(withDictionary dictionary: [String: Any]?) -> Mark? {
        guard let dictionary = dictionary else { return nil }
        var formOfControl: FormOfControl = .unknown
        
        if let formOfControlString = dictionary["formOfControl"] as? String {
            switch formOfControlString {
            case "Зач.":
                formOfControl = .offset
            case "Экзам.":
                formOfControl = .exam
            case "Курс.работа":
                formOfControl = .courseWork
            case "Курс.проект":
            formOfControl = .courseProject
            default:
                formOfControl = .unknown
            }
        }
        
        return Mark(subject: dictionary["subject"] as? String ?? "",
                    formOfControl: formOfControl,
                    mark: dictionary["mark"] as? String ?? "",
                    date: dictionary["date"] as? String ?? "",
                    teacher: dictionary["teacher"] as? String ?? "")
    }
}
