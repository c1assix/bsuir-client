//
//  QuaterMarks.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/6/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

class QuaterMarks {
    let averageMark: Double
    let marks: [Mark]
    let quater: Int
    
    init(averageMark: Double, marks: [Mark], quater: Int) {
        self.averageMark = averageMark
        self.marks = marks
        self.quater = quater
    }
}

extension QuaterMarks {
    static func object(withDictionary dictionary: [String: Any]?, quater: Int) -> QuaterMarks? {
        guard let dictionary = dictionary else { return nil }
        
        var averageMark: Double = 0.0
        
        if let averageMarkString = dictionary["averageMark"] as? NSNumber {
            averageMark = Double(averageMarkString) ?? 0.0
        }
        
        let marksArray = dictionary["marks"] as? [[String: Any]] ?? []
        let marks = marksArray.compactMap({ return Mark.object(withDictionary: $0) })
        
        return QuaterMarks(averageMark: averageMark, marks: marks, quater: quater)
    }
}
