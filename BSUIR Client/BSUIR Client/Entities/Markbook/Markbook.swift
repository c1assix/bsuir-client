//
//  Markbook.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/6/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

class Markbook {
    let averageMark: Double
    let quaterMarks: [QuaterMarks]
    
    init(averageMark: Double, quaterMarks: [QuaterMarks]) {
        self.averageMark = averageMark
        self.quaterMarks = quaterMarks
    }
}

extension Markbook {
//    static func object(withDictionary dictionary: [String: Any]?, quater: Int) -> Markbook? {
//        guard let dictionary = dictionary else { return nil }
//
//        var averageMark: Double = 0.0
//
//        if let averageMarkString = dictionary["averageMark"] as? String {
//            averageMark = Double(averageMarkString) ?? 0.0
//        }
//        let quaterMarksArray = dictionary["markPages"] as? [[String: Any]] ?? []
//        let marks = marksArray.compactMap({ return Mark.object(withDictionary: $0) })
//
//        return Markbook(averageMark: averageMark, marks: marks, quater: quater)
//    }
}

