//
//  Employee.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/30/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class Employee {
    let academicDepartment: String
    let calendarId: URL?
    let fio: String
    let firstName: String
    let id: Int
    let lastName: String
    let middleName: String
    let photoLink: URL?
    let rank: String
    let image: UIImageView
    
    init(academicDepartment: String, calendarId: URL?, fio: String, firstName: String, id: Int, lastName: String, middleName: String, photoLink: URL?, rank: String, image: UIImageView) {
        self.academicDepartment = academicDepartment
        self.calendarId = calendarId
        self.fio = fio
        self.firstName = firstName
        self.id = id
        self.lastName = lastName
        self.middleName = middleName
        self.photoLink = photoLink
        self.rank = rank
        self.image = image
    }
}

extension Employee {
    static func object(withDictionary dictionary: [String: Any]?) -> Employee? {
        guard let dictionary = dictionary else { return nil }
        var photoLink: URL?
        var calendarId: URL?
        let photoLinkString = dictionary["photoLink"] as? String
        let calendarIdString = dictionary["caledarId"] as? String
        if let photoLinkString = photoLinkString {
            photoLink = URL(string: photoLinkString)
        }
        if let calendarIdString = calendarIdString {
            calendarId = URL(string: calendarIdString)
        }
        
        let imageView = UIImageView()
        imageView.kf.setImage(with: photoLink, placeholder:  UIImage(named: "profile-noimage"))
        
        return Employee(academicDepartment: dictionary["academicDepartment"] as? String ?? "",
                        calendarId: calendarId,
                        fio: dictionary["fio"] as? String ?? "",
                        firstName: dictionary["firstName"] as? String ?? "",
                        id: dictionary["id"] as? Int ?? 0,
                        lastName: dictionary["lastName"] as? String ?? "",
                        middleName: dictionary["middleName"] as? String ?? "",
                        photoLink: photoLink,
                        rank: dictionary["rank"] as? String ?? "",
                        image: imageView)
        
    }
}
