//
//  Schedule.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/4/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

enum WeekDay: String {
    case monday = "Понедельник"
    case tuesday = "Вторник"
    case wednesday = "Среда"
    case thursday = "Четверг"
    case friday = "Пятница"
    case saturday = "Суббота"
    case sunday = "Воскресенье"
}

class Schedule {
    let weekDay: WeekDay
    let subjects: [Subject]
    var weekNumber = 1
    
    init(weekDay: WeekDay, subjects: [Subject]) {
        self.subjects = subjects
        self.weekDay = weekDay
    }
}

extension Schedule {
    static func object(withDictionary dictionary: [String: Any]?) -> Schedule? {
        guard let dictionary = dictionary else { return nil }
        var subjects = [Subject]()
        var weekday: WeekDay? = nil
        if let weekdayString = dictionary["weekDay"] as? String {
            switch weekdayString {
                case "Понедельник": weekday = .monday
                case "Вторник": weekday = .tuesday
                case "Среда": weekday = .wednesday
                case "Четверг": weekday = .thursday
                case "Пятница": weekday = .friday
                case "Суббота": weekday = .saturday
                case "Воскресенье": weekday = .sunday
            default:
                break
            }
        }
        if let schedule = dictionary["schedule"] as? [[String: Any]] {
            subjects = schedule.compactMap({ return Subject.object(withDictionary: $0) })
        }
        
        guard let weekdayForSchedule = weekday else { return nil }
        
        return Schedule(weekDay: weekdayForSchedule,
                        subjects: subjects)
    }
}
