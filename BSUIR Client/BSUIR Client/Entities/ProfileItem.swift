//
//  ProfileItem.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/2/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import UIKit

enum ProfileItemIndex {
    case library
    case markbook
    case spravka
    case logout
    case map
}

class ProfileItem {
    let title: String
    let index: ProfileItemIndex
    let image: UIImage?
    
    init(title: String, index: ProfileItemIndex, image: UIImage? = nil) {
        self.title = title
        self.index = index
        self.image = image
    }
}
