//
//  Group.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/5/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

class Group {
    let name: String
    let facultyId: Int
    let id: Int
    var subgroup = 1
    
    init(name: String, facultyId: Int, id: Int) {
        self.facultyId = facultyId
        self.name = name
        self.id = id
    }
}

extension Group {
    static func object(withDictionary dictionary: [String: Any]?) -> Group? {
        guard let dictionary = dictionary else { return nil }
        return Group(name: dictionary["name"] as? String ?? "",
                     facultyId: dictionary["facultyId"] as? Int ?? 0,
                     id: dictionary["id"] as? Int ?? 0)
    }
}
