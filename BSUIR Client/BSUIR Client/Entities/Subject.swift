//
//  Schedule.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/30/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

//class Schedule {
//    static var schedule = [Subject]()
//}

enum LessonType: String {
    case practic = "ПЗ"
    case lecture = "ЛК"
    case lab = "ЛР"
    case none = ""
}

class Subject {
    let subGroupNumber: Int
    let startLessonTime: String
    let endLessonTime: String
    let subject: String
    let lessonType: LessonType
    let employee: [Employee]
    let auditory: [String]
    let weekNumber: [Int]
    
    init(subGroupNumber: Int, startLessonTime: String, endLessonTime: String, subject: String, lessonType: LessonType, employee: [Employee], auditory: [String], weekNumber: [Int]) {
        self.subject = subject
        self.subGroupNumber = subGroupNumber
        self.employee = employee
        self.lessonType = lessonType
        self.startLessonTime = startLessonTime
        self.endLessonTime = endLessonTime
        self.auditory = auditory
        self.weekNumber = weekNumber
    }
}

extension Subject {
    static func object(withDictionary dictionary: [String: Any]?) -> Subject? {
        guard let dictionary = dictionary else { return nil }
        var lessonType: LessonType = .none
        
        if let typeString = dictionary["lessonType"] as? String {
            switch typeString {
            case "ПЗ":
                lessonType = .practic
            case "ЛР":
                lessonType = .lab
            case "ЛК":
                lessonType = .lecture
            default:
                lessonType = .none
            }
        }
        
        var employee = [Employee]()
        
        if let employeeArray = dictionary["employee"] as? [[String: Any]?] {
            employee = employeeArray.compactMap({ return Employee.object(withDictionary: $0) })
        }
        
        return Subject(subGroupNumber: dictionary["numSubgroup"] as? Int ?? 0,
                       startLessonTime:  dictionary["startLessonTime"] as? String ?? "",
                       endLessonTime: dictionary["endLessonTime"] as? String ?? "",
                       subject: dictionary["subject"] as? String ?? "",
                       lessonType: lessonType,
                       employee: employee,
                       auditory: dictionary["auditory"] as? [String] ?? [],
                       weekNumber: dictionary["weekNumber"] as? [Int] ?? [0])
    }
}
