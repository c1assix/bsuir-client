//
//  Student.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/28/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

class Student {
    let birthDate: String
    let course: Int
    let faculty: String
    let id: Int
    let lastName: String
    let middleName: String
    let firstName: String
    let photoUrl: URL?
    let rating: Int
    let studentGroup: Int
    let speciality: String
    let references: [Reference]
    let skills: [Skill]
    
    init(birthDate: String, course: Int, faculty: String, id: Int, lastName: String, middleName: String, firstName: String, photoUrl: URL?, rating: Int, studentGroup: Int, speciality: String, references: [Reference], skills: [Skill]) {
        self.birthDate = birthDate
        self.course = course
        self.faculty = faculty
        self.id = id
        self.lastName = lastName
        self.firstName = firstName
        self.middleName = middleName
        self.rating = rating
        self.photoUrl = photoUrl
        self.studentGroup = studentGroup
        self.speciality = speciality
        self.references = references
        self.skills = skills
    }
}

extension Student {
    static func object(withDictionary dictionary: [String: Any]?) -> Student? {
        guard let dictionary = dictionary else { return nil }
        var photoUrl: URL?
        var references = [Reference]()
        var skills = [Skill]()
        let photoUrlString = dictionary["photoUrl"] as? String
        if let photoUrlString = photoUrlString {
            photoUrl = URL(string: photoUrlString)
        }
        
        if let referencesArray = dictionary["references"] as? [[String: Any]] {
            references = referencesArray.compactMap({ return Reference.object(withDictionary: $0) })
        }
        
        if let skillsArray = dictionary["skills"] as? [[String: Any]] {
            skills = skillsArray.compactMap({ return Skill.object(withDictionary: $0) })
        }
        
        let groupNumber = dictionary["studentGroup"] as? String ?? ""
                
        return Student(birthDate: dictionary["birthDate"] as? String ?? "",
                       course: dictionary["cource"] as? Int ?? 0,
                       faculty: dictionary["faculty"] as? String ?? "",
                       id: dictionary["id"] as? Int ?? 0,
                       lastName: dictionary["lastName"] as? String ?? "",
                       middleName: dictionary["middleName"] as? String ?? "",
                       firstName: dictionary["firstName"] as? String ?? "",
                       photoUrl: photoUrl,
                       rating: dictionary["rating"] as? Int ?? 0,
                       studentGroup: Int(groupNumber) ?? 0,
                       speciality: dictionary["speciality"] as? String ?? "",
                       references: references,
                       skills: skills)
    }
}
