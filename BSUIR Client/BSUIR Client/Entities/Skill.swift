//
//  Skill.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/30/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

class Skill {
    let id: Int
    let name: String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}

extension Skill {
    static func object(withDictionary dictionary: [String: Any]?) -> Skill? {
        guard let dictionary = dictionary else { return nil }
        return Skill(id: dictionary["id"] as? Int ?? 0,
                     name: dictionary["name"] as? String ?? "")
    }
}
