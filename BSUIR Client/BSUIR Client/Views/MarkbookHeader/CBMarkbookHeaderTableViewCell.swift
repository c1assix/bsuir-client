//
//  CBMarkbookHeaderTableViewCell.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/9/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit

class CBMarkbookHeaderTableViewCell: UITableViewCell {

    @IBOutlet private weak var semesterLabel: UILabel!
    @IBOutlet private weak var averageMarkLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setupHeader(with markbook: Markbook?, quoter: Int) {
        if let markbook = markbook {
            self.semesterLabel.text = " Cеместр " + String(markbook.quaterMarks[quoter].quater)
            self.averageMarkLabel.text = String(format: "%0.2f", markbook.quaterMarks[quoter].averageMark)
        }
        self.backgroundColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
