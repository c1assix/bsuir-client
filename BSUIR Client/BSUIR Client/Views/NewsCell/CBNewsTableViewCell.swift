//
//  CBNewsTableViewCell.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/29/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import Kingfisher

class CBNewsTableViewCell: UITableViewCell {

    @IBOutlet private weak var newsImage: UIImageView!
    @IBOutlet private weak var newsTitle: UILabel!
    @IBOutlet private weak var topicLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func setupCell(with news: News) {
        self.newsImage.kf.setImage(with: URL(string: news.image))
        self.newsTitle.text = news.title
        self.topicLabel.text = news.topic
        self.dateLabel.text = news.date
    }
    
}

class News {
    
    static var news = [News]()
    
    let title: String
    let image: String
    let topic: String
    let date: String
    let url: String
    
    init(title: String, image: String, topic: String, date: String, url: String) {
        self.title = title
        self.topic = topic
        self.date = date
        self.image = image
        self.url = url
    }
}

extension News {
    static func generateNews() {
        self.news = [News(title: "Команда БГУИР вышла в финал Чемпионата мира по программированию", image: "https://pp.userapi.com/c845421/v845421818/148eaf/ZtwZdcG0mf8.jpg", topic: "ФКСиС", date: "6 декабря", url: "https://vk.com/bsuir_official?w=wall-143039548_4474"),
                     News(title: "БГУИР принял участие в выставке-форуме ITE-2018", image: "https://pp.userapi.com/c845421/v845421818/148e4d/R8Oscw3MjhY.jpg", topic: "БГИУР", date: "5 декабря", url: "https://vk.com/bsuir_official?w=wall-143039548_4470"),
                     News(title: "Выпускница БГУИР, которая заработала в США первый миллион", image: "https://pp.userapi.com/c846521/v846521585/13cc74/xrcI10TWa48.jpg", topic: "БГИУР", date: "27 ноября", url: "https://vk.com/bsuir_official?w=wall-143039548_4425"),
                     News(title: "В БГУИР выбрали лучшие работы для конкурса '100 идей для Беларуси'", image: "https://pp.userapi.com/c850616/v850616720/57e3e/XeioWiRuWPU.jpg", topic: "БГИУР", date: "27 ноября", url: "https://vk.com/bsuir_official?w=wall-143039548_4422"),
                     News(title: "Выпускники МРТИ-БГУИР встретились с администрацией университета", image: "https://pp.userapi.com/c851328/v851328811/4a44f/IhoGIDcJR4A.jpg", topic: "БГИУР", date: "20 ноября", url: "https://vk.com/bsuir_official?w=wall-143039548_4368"),
                     News(title: "Одаренные белорусские школьники посетили БГУИР", image: "https://pp.userapi.com/c844416/v844416354/13882e/yMrFaIrXcd0.jpg", topic: "БГИУР", date: "19 ноября", url: "https://vk.com/bsuir_official?w=wall-143039548_4363"),
                     News(title: "БГУИР стал участником форума 'Студенческая осень - 2018'", image: "https://pp.userapi.com/c849236/v849236847/bab67/VkTmxX2t5ns.jpg", topic: "БГИУР", date: "16 ноября", url: "https://vk.com/bsuir_official?w=wall-143039548_4351")]
    }
}
