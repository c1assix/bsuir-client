//
//  CBEventTableViewCell.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/6/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import Kingfisher

class CBEventTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var dateNumberLabel: UILabel!
    @IBOutlet private weak var monthNameLabel: UILabel!
    @IBOutlet private weak var typeNameLable: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func styleCell(with event: Event) {
        self.dateNumberLabel.text = event.dateNumber
        self.monthNameLabel.text = event.date
        self.typeNameLable.text = event.type
        self.titleLabel.text = event.title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
