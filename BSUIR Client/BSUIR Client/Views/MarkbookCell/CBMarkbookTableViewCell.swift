//
//  CBMarkbookTableViewCell.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/9/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit

class CBMarkbookTableViewCell: UITableViewCell {

    @IBOutlet private weak var markLabel: UILabel!
    @IBOutlet private weak var subjectNameLabel: UILabel!
    @IBOutlet private weak var teacherName: UILabel!
    @IBOutlet private weak var teacherLabelConstraint: NSLayoutConstraint!
    @IBOutlet private weak var formOfControlLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func styleCell(with mark: Mark?) {
        if let mark = mark {
            self.markLabel.text = mark.mark
            if mark.teacher != "" {
                self.teacherName.text = mark.teacher
                self.teacherLabelConstraint.constant = 17
            } else {
                self.teacherLabelConstraint.constant = 0
            }
            self.markLabel.textColor = UIColor(red: 0.0 / 255.0, green: 102.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            if let markInt = Int(mark.mark) {
                if markInt < 5 {
                    self.markLabel.textColor = .red
                }
            }
            self.subjectNameLabel.text = mark.subject
            self.formOfControlLabel.text = mark.formOfControl.rawValue
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
