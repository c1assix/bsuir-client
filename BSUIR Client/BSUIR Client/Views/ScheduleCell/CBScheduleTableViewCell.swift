//
//  CBScheduleTableViewCell.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 11/30/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit
import Kingfisher

class CBScheduleTableViewCell: UITableViewCell {

    @IBOutlet private weak var subjectTypeView: UIView!
    @IBOutlet private weak var subjectTimeLabel: UILabel!
    @IBOutlet private weak var subjectName: UILabel!
    @IBOutlet private weak var auditory: UILabel!
    @IBOutlet private weak var employeeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func styleCell(subject: Subject) {
        self.auditory.text = subject.auditory.first
        self.subjectTimeLabel.text = subject.startLessonTime + "\n-\n" + subject.endLessonTime
        self.subjectName.text = subject.subject
        self.employeeImage?.image = subject.employee.first?.image.image
        switch subject.lessonType {
        case .lab:
            self.subjectTypeView.backgroundColor = .blightGrey
        case .lecture:
            self.subjectTypeView.backgroundColor = .carolinaBlue
        case .practic:
            self.subjectTypeView.backgroundColor = .darkBlue
        case .none:
            break
        }
        self.employeeImage.layer.cornerRadius = self.employeeImage.bounds.height / 2
        self.employeeImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
