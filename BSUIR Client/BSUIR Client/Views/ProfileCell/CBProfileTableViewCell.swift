//
//  CBProfileTableViewCell.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/9/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import UIKit

class CBProfileTableViewCell: UITableViewCell {

    @IBOutlet private weak var iconImage: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func styleCell(item: ProfileItem) {
        self.iconImage.image = item.image
        self.titleLabel.text = item.title
        self.iconImage.tintColor = .darkBlue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
