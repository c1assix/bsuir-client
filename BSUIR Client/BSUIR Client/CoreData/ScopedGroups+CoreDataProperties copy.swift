//
//  ScopedGroups+CoreDataProperties.swift
//  
//
//  Created by Лев Купчинов on 12/8/19.
//
//

import Foundation
import CoreData


extension ScopedGroups {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ScopedGroups> {
        return NSFetchRequest<ScopedGroups>(entityName: "ScopedGroups")
    }

    @NSManaged public var groupNumber: String?
    @NSManaged public var subGroupNumber: String?

}
