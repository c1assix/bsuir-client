//
//  NotificationName.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/9/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let darklayerIsNeeded = Notification.Name("darklayerIsNeeded")
    static let darklayerIsNotNeeded = Notification.Name("darklayerIsNotNeeded")
    
    static let updateSchedule = Notification.Name("updateSchedule")
}
