//
//  UIColor.swift
//  BSUIR Client
//
//  Created by Лев Купчинов on 12/9/19.
//  Copyright © 2018 Лев Купчинов. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    @nonobjc class var blightGrey: UIColor {
        return UIColor(red: 172.0 / 255.0, green: 172.0 / 255.0, blue: 172.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var darkBlue: UIColor {
        return UIColor(red: 31.0 / 255.0, green: 57.0 / 255.0, blue: 143.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var carolinaBlue: UIColor {
        return UIColor(red: 125.0 / 255.0, green: 151.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    }
    
}
